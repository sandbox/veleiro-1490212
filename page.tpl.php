<div id="page">
	<div id="header-wrapper">
		<div id="header">
			
			  <div id="region-header">
			  <?php print render($page['header']); ?>
			  </div><!--/region-header -->
			  
			
			<div id="header-in-l">
			<?php if ($logo): ?>
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
			<?php endif; ?>
			<?php if ($site_name || $site_slogan): ?>
            <hgroup id="name-and-slogan">
              <?php if ($site_name): ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
              <?php endif; ?>
			  <?php if ($site_slogan): ?>
              <h2 id="site-slogan"><?php print $site_slogan; ?></h2>
              <?php endif; ?>
            </hgroup><!-- /#name-and-slogan -->
            <?php endif; ?>
			  <div id="region-menu">
			  <?php print render($page['menu']); ?>
			  </div><!--/region-menu -->
			</div><!--/header-in-r -->
		</div><!--/header -->
	</div><!--/header-wrapper -->
	<div id="main-wrapper">
	  <div id="main-bg">
	   <div id="main-bg2">
		<div id="main">
		  <?php print render($page['highlighted']); ?>
		  <div id="sidebar">
		  <img src="http://raleightile.us/sites/all/themes/guardian/images/logo.png" alt="Raleigh Tile" />
		    <div id="side-1">
		    <?php print render($page['sidebar_first']); ?>
		    </div>
		    <div id="side-2">
		    <?php print render($page['sidebar_second']); ?> 
		    </div>
			<?php
drupal_add_library('system', 'ui.draggable');
drupal_add_js('jQuery(document).ready(function(){jQuery("#side-3").draggable();});', 'inline');
?>
<script type="text/javascript">
	jQuery(function() {
		jQuery( "#side-3" ).draggable({ axis: "y" });
	});
	jQuery(function() {
		jQuery( "#side-3" ).draggable({ containment: "#side-3-w", scroll:false });
	});
	</script>
			<div id="side-3-w">
		    <div id="side-3">
		    <?php print render($page['sidebar_third']); ?>
		    </div>
			</div>
		  </div>
		  <div id="content-wrapper">
		  <?php if ($breadcrumb || $tabs): ?>
		  <div id="content-t">
		  <?php if ($breadcrumb): ?>
          <div id="breadcrumb"><?php print $breadcrumb; ?></div>
          <?php endif; ?>
		  <?php if ($tabs): ?>
          <div class="tabs">
           <?php print render($tabs); ?>
          </div>
          <?php endif; ?>
		  </div>
		  <?php endif; ?>
		  <div id="content">
            <?php print render($title_prefix); ?>
            <?php if ($title): ?>
            <h1 class="title" id="page-title">
              <?php print $title; ?>
            </h1>
           <?php endif; ?>
           <?php print render($title_suffix); ?>
           <?php if ($action_links): ?>
           <ul class="action-links">
             <?php print render($action_links); ?>
           </ul>
           <?php endif; ?>
		  <?php print render($page['help']); ?>
		  <?php print render($page['content']); ?>
		  </div><!--/content-->
		  </div><!--/content-wrapper-->
		</div><!--/main -->
	  </div><!--/main-bg2 -->
	 </div><!--/main-bg -->
	</div><!--/main-wrapper -->
	<div id="footer-wrapper">
		<div id="footer">
		  <div id="foot1" class="foot">
		    <?php print render($page['footer_first']); ?>
		  </div><!--/foot1 -->
		  <div id="foot2" class="foot">
		    <?php print render($page['footer_second']); ?>
		  </div><!--/foot2 -->
		  <div id="foot3" class="foot">
		    <?php print render($page['footer_third']); ?>
		  </div><!--/foot3 -->
		  <div id="foot-msg">
		    <?php print render($page['footer_message']); ?>
		  </div><!--/footer-msg-->
		</div><!--/footer -->
	</div><!--/footer-wrapper -->
</div> <!-- /page-->